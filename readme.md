# bunnemoji 2.1

a collection of cute bunne emoji, free for non commercial use. <a href="./bunnemoji.zip">download the zip</a>.

## changes in 2.1

- added bunneawwheart, bunnetransheart, bunnewawawa, bunnecry and bunnesad

<img width="540" src="./bunnemoji.png">