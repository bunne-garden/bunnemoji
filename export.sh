#!/usr/bin/fish
if count export/*.png >/dev/null
    rm export/*.png
end

for emoji in emoji/*.png
    magick $emoji -resize 128 -strip export/$(basename $emoji)
end

montage emoji/*.png -tile 4x -geometry '250x250+10+10>' -pointsize 34 -set label '\n%t' bunnemoji.png

zip -j bunnemoji.zip export/*.png
